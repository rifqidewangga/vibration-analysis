import numpy as np
from numpy import arange, array, sqrt, mean, square
from numpy.linalg import norm
from matplotlib import pyplot as plt
from scipy.fftpack import fft, ifft
import glob
from natsort import natsorted

sample_rate = 0.001  # seconds


def get_raw_data(sampling_rate, file):
	raw = np.genfromtxt(file, delimiter=",")
	raw_average = np.average(raw, axis=0)
	x_acceleration = raw[:, ][:, 0] - raw_average[0]
	y_acceleration = raw[:, ][:, 1] - raw_average[1]
	z_acceleration = raw[:, ][:, 2] - raw_average[2]
	data_length = len(x_acceleration)
	time = arange(0, sampling_rate * data_length, sampling_rate)
	# resultant = norm([x_acceleration, y_acceleration, z_acceleration], axis=0)
	resultant = sqrt(x_acceleration**2 + y_acceleration**2 + z_acceleration**2)

	if len(time)-len(x_acceleration) > 0:
		return array([time[:-1], x_acceleration, y_acceleration, z_acceleration, resultant])
	else:
		return array([time, x_acceleration, y_acceleration, z_acceleration, resultant])


def process_fft(sampling_rate, raw_data):
	N = len(raw_data[1])

	x_acc_f = fft(raw_data[1, :])
	y_acc_f = fft(raw_data[2, :])
	z_acc_f = fft(raw_data[3, :])
	resultant_acc_f = fft(raw_data[4, :])
	tf = np.linspace(0.0, 1.0 / (2.0 * sampling_rate), N//2)

	return array([tf, x_acc_f, y_acc_f, z_acc_f, resultant_acc_f])


def plot_data(acceleration_data, fft_data, i):
	plt.figure(i, figsize=[19, 8])
	plt.tight_layout()

	x_low_limit = 0

	plot_time_domain_ref = plt.subplot(321)
	plt.plot(acceleration_data[0, :], acceleration_data[1, :], 'r', label="x acceleration")  # x_acceleration
	# plt.xlim(left=x_low_limit)
	plt.title("Vibration Measurement (time domain)")
	plt.legend(loc='upper right')
	plt.grid()
	plt.ylabel("acceleration (m/s^2)")

	plt.subplot(323, sharex=plot_time_domain_ref, sharey=plot_time_domain_ref)
	plt.plot(acceleration_data[0, :], acceleration_data[2, :], 'g', label="y acceleration")  # y_acceleration
	plt.legend(loc='upper right')
	plt.grid()
	plt.ylabel("acceleration (m/s^2)")

	plt.subplot(325, sharex=plot_time_domain_ref, sharey=plot_time_domain_ref)
	plt.plot(acceleration_data[0, :], acceleration_data[3, :], 'b', label="z acceleration")  # z_acceleration
	plt.legend(loc='upper right')
	plt.grid()
	plt.xlabel("time (s)")
	plt.ylabel("acceleration (m/s^2)")
	
	axes = plt.gca()
	axes.set_xlim([0, None])
	axes.set_ylim([-2.5, 2.5])

	# Frequency Domain Plot
	x_low_limit = 0
	x_high_limit = 500
	y_low_limit = 0

	N = len(acceleration_data[1])
	plot_freq_domain_ref = plt.subplot(322)
	# plt.xlim(left=x_low_limit, right=x_high_limit)
	# plt.ylim(bottom=y_low_limit)
	plt.autoscale(axis='y')
	plt.plot(fft_data[0][:], 2.0 / N * np.abs(fft_data[1][:N//2]), 'r', label="x acceleration")  # x_acceleration
	plt.title("Vibration Measurement (frequency domain)")
	plt.legend(loc='upper right')
	plt.grid()
	plt.ylabel("acceleration (m/s^2)")

	plt.subplot(324, sharex=plot_freq_domain_ref, sharey=plot_freq_domain_ref)
	plt.plot(fft_data[0][:], 2.0 / N * np.abs(fft_data[2][:N//2]), 'g', label="y acceleration")  # y_acceleration
	plt.legend(loc='upper right')
	plt.grid()
	plt.ylabel("acceleration (m/s^2)")

	plt.subplot(326, sharex=plot_freq_domain_ref, sharey=plot_freq_domain_ref)
	plt.plot(fft_data[0][:], 2.0 / N * np.abs(fft_data[3][:N//2]), 'b', label="z acceleration")  # z_acceleration
	plt.legend(loc='upper right')
	plt.grid()
	plt.ylabel("acceleration (m/s^2)")
	plt.xlabel("frequency (Hz)")
	
	axes = plt.gca()
	axes.set_xlim([0, 500])
	axes.set_ylim([0, 1.2])


def rms(data):
	rms_value = sqrt(mean(square(data)))
	return rms_value


def calculate_rms_and_max(filelist):
	statistic = open("Statistic.csv", 'w')
	statistic.write("rover velocity (mm/s), x acceleration RMS (m/s2), y acceleration RMS (m/s2), z acceleration RMS (m/s2), RMS resultant (m/s2), X_max (m/s2), Y_max (m/s2), Z_max (m/s2), Max Resultant (m/s2)\n")
	for i in filelist:
		if i != "noisefloor.csv":
			raw_data = get_raw_data(sample_rate, i)
			x_rms = rms(raw_data[1])
			y_rms = rms(raw_data[2])
			z_rms = rms(raw_data[3])
			res_rms = sqrt(x_rms**2 + y_rms**2 + z_rms**2)
			x_max = np.max (raw_data[1])
			y_max = np.max (raw_data[2])
			z_max = np.max (raw_data[3])
			res_max = sqrt(x_max**2 + y_max**2 + z_max**2)

			statistic.write(i[:-4] + ", " + str(x_rms) + ", " + str(y_rms) + ", " + str(z_rms) + ", " + str(res_rms) +  ", " + str(x_max) +  ", " + str(y_max) +  ", " + str(z_max) + "," + str(res_rms) + "\n")
	statistic.close()


def main():
	filelist = natsorted(glob.glob("*.csv"))

	# Calculate RMS and Maximum acceleration, write it to Statistics.txt
	calculate_rms_and_max(filelist)

	# plot data
	for filename in filelist:
		filename_no_extension = filename[:-4]
		if filename != "noisefloor.csv":
			test = get_raw_data(sample_rate, filename)
			fft_test = process_fft(sample_rate, test)
			plot_data(test, fft_test, filename)
			figure_name = filename_no_extension + ".png"
			plt.savefig(figure_name, dpi=300, bbox_inches='tight')

	plt.show()


if __name__== "__main__":
	main()
