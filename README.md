Step to use this script:

1. Prepare raw data taken from accelerometer each in csv file and make sure column 1, 2, and 3 are corresponding to x, y and z axis.
2. Set sample rate according to your measurements, in this example the sample rate is set to 0.001 second
3. Make sure following libraries are installed: numpy, matplotlib, scipy, glob and natsort
4. The script will automatically process data to frequency and time domain, save graph image of each dataset and basic statistic of rms value, max and resultant of every axis.